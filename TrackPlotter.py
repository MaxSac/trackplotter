import numpy as np


def calc_parametrisation(x0, x1):
    """ Calculates the parmetrisation for the line equation p_1*lambda + p_2.
    Parameters:
        x0, x1: np.ndarray
    Returns:
        a, b: np.ndarray
            vectors how span the line
        lambda_max: float
            upper limit on line
    """

    lambda_max = np.linalg.norm(x1 - x0)
    p_1 = (x1 - x0) / lambda_max
    p_2 = x0
    return [p_1, p_2, lambda_max]


def distance(param, point):
    """ Calculates the distance of a point to a given straight track.
    https://en.wikipedia.org/wiki/Distance_from_a_point_to_a_line#Vector_formulation
    """
    if not 0 <= (point - param[1]) @ param[0].T <= param[2]:
        return np.inf
    # return np.linalg.norm(
    #     (param[1] - point) - ((param[1] - point) @ param[0].T) * param[0]
    # )
    return (param[1] - point) - ((param[1] - point) @ param[0].T) * param[0]

class stack:
    def __init__(self, dim):
        self.proofed = np.zeros(dim)
        self.to_proof = []

    def add(self, pos, indices):
        if self.proofed[indices[0], indices[1], indices[2]] == 0:
            self.to_proof.append(pos)
            self.proofed[indices[0], indices[1], indices[2]] = True

    def pop(self):
        return self.to_proof.pop()

    def empty(self):
        if not self.to_proof:
            return True
        return False


class TrackPlotter:
    def _calc_boundaries(self):
        self.minimum = np.array([])
        self.maximum = np.array([])
        self.binwidth = np.array([])
        self.dimension = np.array([], dtype=np.int)
        for mesh in self.meshgrid:
            self.minimum = np.append(self.minimum, min(mesh.squeeze()))
            self.maximum = np.append(self.maximum, max(mesh.squeeze()))
            self.dimension = np.append(self.dimension, len(mesh.squeeze()))
            self.binwidth = np.append(self.binwidth, min(np.diff(mesh.squeeze())))

    def __init__(self, meshgrid):
        self.meshgrid = meshgrid
        self._calc_boundaries()
        self.hist = np.zeros(self.dimension)

    def calc_neighbours(self, indices):
        """ Calculates the neighbours to a given position """
        neighbours = []
        for i in range(0, 27):
            n_bin = (
                np.array([(i // 9) - 1, (i // 3) % 3 - 1, (i % 3) - 1]) + indices
            )
            if (n_bin < np.zeros(3)).any() or (n_bin >= self.dimension).any():
                continue
            neighbours.append(n_bin)
        return neighbours

    def in_boundaries(self, pos):
        return (self.minimum <= pos).all() and (self.maximum >= pos).all()

    def calc_indices(self, pos):
        indices = []
        for x_i, ax_pos in zip(pos, self.meshgrid):
            indices.append(np.argmin(np.abs(ax_pos - x_i)))
        return indices

    def calc_crossed_pixel(self, x0, x1):
        param = calc_parametrisation(np.array(x0), np.array(x1))
        s = stack(self.dimension)
        s.add(param[1], self.calc_indices(param[1]))
        track = []
        while not s.empty():
            pos = s.pop()
            if (np.abs(distance(param, pos)) <= self.binwidth / 2).all():
                indices = self.calc_indices(pos)
                track.append(indices)
                for n_indices in self.calc_neighbours(indices):
                    pos = np.array(
                        [
                            self.meshgrid[0].squeeze()[n_indices[0]],
                            self.meshgrid[1].squeeze()[n_indices[1]],
                            self.meshgrid[2].squeeze()[n_indices[2]],
                        ]
                    )
                    if self.in_boundaries(pos):
                        s.add(pos, n_indices)
        return track

    def hist_track(self, x0, x1):
        crossed_bins = self.calc_crossed_pixel(x0, x1)
        for crossed_bin in crossed_bins:
            self.hist[crossed_bin[0], crossed_bin[1], crossed_bin[2]] += 1
        return self.hist


if __name__ == "__main__":
    import pandas as pd

    df = pd.DataFrame(
        {
            # "x0": [[0, 0, 0], [5, 5, 0], [0, 0, 0]],
            # "x1": [[0, 10, 0], [5, 5, 10], [10, 10, 10]],
            "x0": [[0, 0, 0]],
            "x1": [[10, 10, 10]],
        }
    )

    track_plttr = TrackPlotter(
        np.meshgrid(
            np.linspace(0, 10, 11),
            np.linspace(0, 10, 11),
            np.linspace(0, 13, 13),
            sparse=True,
        )
    )

    for x0, x1 in zip(df.x0, df.x1):
        track_plttr.hist_track(x0, x1)

    hist = np.sum(track_plttr.hist, axis=0)
    print(hist)
